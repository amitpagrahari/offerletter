<?php 
	$title = "Offer Letter PDF Generation";
	$meta_desc = "Offer Letter PDF Generation";
	include_once 'header.php';
	
	$sqlObj = new sqlWrappers();

	if( isset($_GET['user_id']) && !empty($_GET['user_id']) ) {
		$tableParam = array(
							'ol_details' => 
								array (
									'get_cols' => array()
									),
							'candidate' => 
								array (
									'get_cols' => array(),
									'where_cols' => array(
												array('cols' => 'user_id' , 'val' => $_GET['user_id'], 'cond'=> '=')
												)
									)
							);
		$joinsName = array( 
						array('name' => array('INNER JOIN'=>'user_id,id')
						)
				);
		$getStructureDetails = $sqlObj->sqlFetch($tableParam, '', $joinsName);
		$userData = array();
		if( !empty($getStructureDetails) ) {
			foreach ($getStructureDetails as $value) {
				$userData['name'] = $value['f_name'].' '.$value['l_name'];
				$userData['designation'] = $value['designation'];
				$userData['department'] = $value['department'];
				$userData['ctc'] = $value['ctc'];
				$userData[$value['component']] = $value['comp_val'];
			}
		} else {
			header("Location: create-offer-letter.php");
		}
	} else {
		header("Location: create-offer-letter.php");
	}

	
?>


<div class="col-md-12">&nbsp;</div>
<div class="col-md-12">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h2>Offer Letter</h2><br>

		<b>Dear Mr. <?php echo $userData['name']; ?></b><br><br>
		<b>Sub: Employment Offer</b><br>
		<p>With reference to your discussion with us, we are pleased to extend the employment offer to you with the following terms
		and conditions:</p>
		<ul>
			<li>You will be appointed as a <?php echo $userData['designation']; ?> of  <?php echo $userData['department']; ?> Department  with SVG Media Pvt. Ltd. Your initial place of service will be Gurgaon. However, your services are transferable and you can be seconded or deputed by the company to any SVG Media operation in India or abroad.</li>
			<li> Your date of joining with our organization will be “1 st March’2016”.</li>
			<li>Your Annual CTC will be Rs. <?php echo $userData['ctc']; ?> /-</li>
		</ul>

		<p>The following has to be submitted by you at the time of your joining:</p>
		<ul>
			<li>Copy of Educational Qualification Certificates.</li>
			<li>Recent 6 nos photographs</li>
			<li>Copy of all Experience (Last employer’s appointment letter etc).</li>
			<li>Reliving Letter/acceptance of resignation from previous employer.</li>
			<li>Proof of Age & Residence (copy of passport election ID card/ Driving License)</li>
			<li>Proof of last salary drawn / pay slip (last salary slip along with salary certificate / last compensation revision letter) </li>
			<li> Copy of Permanent Account Number (PAN) card.</li>
		</ul>

		<p>This offer of employment will be valid for acceptance by you for a period of 24 hours from the time of receiving the same.</p>

		<h4><b>Details of compensation</b></h4>
		<table border="1">
			<thead>
				<tr>
					<th style="padding:10px;">Component</th>
					<th style="padding:10px;">Salary Structure</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="padding:10px;">Basic</td>
					<td style="padding:10px;"><?php echo $userData['basic']; ?></td>
				</tr>
				<tr>
					<td style="padding:10px;">HRA</td>
					<td style="padding:10px;"><?php echo $userData['hra']; ?></td>
				</tr>
				<tr>
					<td style="padding:10px;">Conveyance Allowance</td>
					<td style="padding:10px;"><?php echo $userData['ca']; ?></td>
				</tr>
				<tr>
					<td style="padding:10px;">Medical Allowance</td>
					<td style="padding:10px;"><?php echo $userData['ma']; ?></td>
				</tr>
				<tr>
					<td style="padding:10px;">Special Allowance</td>
					<td style="padding:10px;"><?php echo $userData['sa']; ?></td>
				</tr>				
				<tr>
					<td style="padding:10px;">CTC</td>
					<td style="padding:10px;"><?php echo $userData['ctc']; ?></td>
				</tr>
			</tbody>
		</table>
		<br><br>
		<p>I agree to accept the employment on the terms and conditions mentioned in the letter.</p>
		<p>Signature:</p>
		<p>Date:</p>
		<br><br>
		<a href="edit-ol.php?user_id=<?php echo $_GET['user_id']; ?>"><button class="btn btn-primary">Edit</button></a>
		<a href="generate-ol-pdf.php?user_id=<?php echo $_GET['user_id']; ?>"><button class="btn btn-primary">Generate PDF</button></a>
	</div>
	<div class="col-md-3"></div>		
</div>
<div class="col-md-12">&nbsp;</div>


<?php 
	include_once 'footer.php';
?>
