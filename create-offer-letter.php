<?php 
	$title = "Offer Letter Generation";
	$meta_desc = "Offer Letter Generation";
	include_once 'header.php';
	$errorMsg = '';
	$sqlObj = new sqlWrappers();
	$tableParam = array(
						'structure' => 
							array (
								'get_cols' => array('id','type')
								)
						);
	$getStructures = $sqlObj->sqlFetch($tableParam);
	if( !empty($_POST) ) {
		if( !empty($_POST['f_name']) && !empty($_POST['l_name']) && !empty($_POST['designation']) && !empty($_POST['department']) && !empty($_POST['structure']) && !empty($_POST['ctc']) ) {
			$user_id = md5(microtime().rand(1,100000));
			$values = array( 
							array($user_id,$_POST['f_name'],$_POST['l_name'],$_POST['designation'],$_POST['department'],$_POST['structure'],$_POST['ctc'])
							);
			$sqlObj->sqlInsertOrReplace("INSERT","candidate",array('user_id','f_name','l_name','designation','department','struct_id','ctc'),$values);
			$tableParam2 = array(
								'structure_details' => 
									array (
										'get_cols' => array(),
										'where_cols' => array(
													array('cols' => 'str_id' , 'val' => (int)$_POST['structure'], 'cond'=> '=' )
													)
										)
								);
			$getStructureDetails = $sqlObj->sqlFetch($tableParam2);

			$tableParam3 = array(
								'candidate' => 
									array (
										'get_cols' => array('id'),
										'where_cols' => array(
													array('cols' => 'user_id' , 'val' => $user_id, 'cond'=> '=' )
													)
										)
								);
			$getID = $sqlObj->sqlFetch($tableParam3);
			$getStructureDetailsData = array();
			foreach ($getStructureDetails as $valStructureDetails) {
				$getStructureDetailsData[$valStructureDetails['component']] = $valStructureDetails['comp_val'];
			}

			$basic = ((int)$_POST['ctc']*(int)$getStructureDetailsData['basic'])/100;
			$hra = ((int)$_POST['ctc']*(int)$getStructureDetailsData['hra'])/100;
			$ca = (int)$getStructureDetailsData['ca'];
			$ma = (int)$getStructureDetailsData['ma'];
			$sa = (int)$_POST['ctc'] - ($basic+$hra+$ca+$ma);
			if( $sa < 0 ) {
				$sa = 0;
				$errorMsg = '<div class="alert alert-danger">Add Sufficent CTC. Special Allowemce Going Negative.</div>';
			} else {
				$insertData = array(
						array($getID[0]['id'],'basic',$basic),
						array($getID[0]['id'],'hra',$hra),
						array($getID[0]['id'],'ca',$ca),
						array($getID[0]['id'],'ma',$ma),
						array($getID[0]['id'],'sa',$sa)
					);

				$sqlObj->sqlInsertOrReplace("INSERT","ol_details",array('user_id','component','comp_val'),$insertData);
				header("Location: ol-details.php?user_id=".$user_id);
			}
		} else {
			$errorMsg = '<div class="alert alert-danger">Please Fill All Fields.</div>';
		}
	}

?>


<div class="col-md-12">&nbsp;</div>
<div class="col-md-12">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h2>Generate Offer Letter</h2><br>
		<?php echo $errorMsg; ?>
		<form action="" method="post">
			<div class="form-group">
				<label>First Name</label>
				<input type="text" class="form-control" id="f_name" name="f_name" placeholder="Jane">
			</div>
			<div class="form-group">
				<label>Last Name</label>
				<input type="text" class="form-control" id="l_name" name="l_name" placeholder="Doe">
			</div>
			<div class="form-group">
				<label>Designation</label>
				<input type="text" class="form-control" id="designation" name="designation" placeholder="PHP Developer">
			</div>
			<div class="form-group">
				<label>Department</label>
				<input type="text" class="form-control" id="department" name="department"  placeholder="Technical">
			</div>			
			<div class="form-group">
				<label>CTC</label>
				<input type="text" class="form-control" id="ctc" name="ctc"  placeholder="1000000">
			</div>			
			<div class="form-group">
				<label>Select Structure</label>
				<select name="structure" class="form-control">
					<?php
						if( !empty($getStructures) ) {
							foreach ($getStructures as $value) {
								echo '<option value="'.$value['id'].'">'.$value['type'].'</option>';
							}
						}
					?>
				</select>
			</div><br>
		  <button type="submit" name="submit" class="btn btn-primary">Generate</button>
		</form>
	</div>
	<div class="col-md-3"></div>		
</div>
<div class="col-md-12">&nbsp;</div>


<?php 
	include_once 'footer.php';
?>