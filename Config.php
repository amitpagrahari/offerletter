<?php
#-----------------------------
define("LOG_LEVEL_FOR_WRITE",5);
define("MYSQL_RETRY_NUMBER",1);


class Configs {

    function sqlClauseList() {
        return array('IN','NOT IN');
    }
    function sqlClauseListOther() {
        return array('IS NULL');
    }
    function insertOrReplaceList() {
        return array('INSERT','REPLACE','INSERT DELAYED');
    }
}

?>
