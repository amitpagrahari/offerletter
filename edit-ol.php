<?php 
	$title = "Offer Letter PDF Generation";
	$meta_desc = "Offer Letter PDF Generation";
	include_once 'header.php';
	
	$sqlObj = new sqlWrappers();
	$errorMsg = '';
	if( isset($_GET['user_id']) && !empty($_GET['user_id']) ) {
		$tableParam = array(
							'ol_details' => 
								array (
									'get_cols' => array()
									),
							'candidate' => 
								array (
									'get_cols' => array(),
									'where_cols' => array(
												array('cols' => 'user_id' , 'val' => $_GET['user_id'], 'cond'=> '=')
												)
									)
							);
		$joinsName = array( 
						array('name' => array('INNER JOIN'=>'user_id,id')
						)
				);
		$getStructureDetails = $sqlObj->sqlFetch($tableParam, '', $joinsName);
		$userData = array();
		if( !empty($getStructureDetails) ) {
			foreach ($getStructureDetails as $value) {
				$userData['f_name'] = $value['f_name'];
				$userData['id'] = $value['id'];
				$userData['l_name'] = $value['l_name'];
				$userData['designation'] = $value['designation'];
				$userData['department'] = $value['department'];
				$userData['ctc'] = $value['ctc'];
				$userData[$value['component']] = $value['comp_val'];
			}
		} else {
			header("Location: create-offer-letter.php");
		}
	} else {
		header("Location: create-offer-letter.php");
	}
	if( !empty($_POST) ) {
		if( ((int)$_POST['basic']+(int)$_POST['hra']+(int)$_POST['ca']+(int)$_POST['ma']+(int)$_POST['sa']) < (int)$_POST['ctc'] ) {
		
			$updateVal = array( 'f_name'=>$_POST['f_name'],'l_name'=>$_POST['l_name'],'designation'=>$_POST['designation'],'department'=>$_POST['department'],'ctc'=>$_POST['ctc']);
			$where = array( 'cond' => array('id'=>$userData['id']));
			$sqlObj->sqlUpdate('candidate',$updateVal,$where = '' );

			$mysqlQuery = "update ol_details
				     set comp_val = 
				     case 
				     when component = 'basic' then ".$_POST['ba']."
				     when component = 'hra' then ".$_POST['hra']."
				     when component = 'ca' then ".$_POST['ca']."
				     when component = 'ma' then ".$_POST['ma']."
				     when component = 'sa' then ".$_POST['sa']."
				     end
				     WHERE user_id=".$userData['id'];
			mysqlRetryLogic($mysqlQuery,__FUNCTION__);
			header("Location: edit-ol.php?user_id=".$_GET['user_id']);
		} else {
			$errorMsg = '<div class="alert alert-danger">Please Add Valid CTC.</div>';
		}
		$errorMsg = '<div class="alert alert-success">Offer Edited Sucessfully.</div>';
	}
	
?>


<div class="col-md-12">&nbsp;</div>
<div class="col-md-12">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<h2>Edit Offer Letter</h2><br>
		<?php echo $errorMsg; ?>
		<form action="" method="post">
			<div class="form-group">
				<label>First Name</label>
				<input type="text" class="form-control" id="f_name" name="f_name" value="<?php echo $userData['f_name']; ?>" placeholder="Jane">
			</div>
			<div class="form-group">
				<label>Last Name</label>
				<input type="text" class="form-control" id="l_name" name="l_name" value="<?php echo $userData['l_name']; ?>" placeholder="Doe">
			</div>
			<div class="form-group">
				<label>Designation</label>
				<input type="text" class="form-control" id="designation" name="designation" value="<?php echo $userData['designation']; ?>" placeholder="PHP Developer">
			</div>
			<div class="form-group">
				<label>Department</label>
				<input type="text" class="form-control" id="department" name="department" value="<?php echo $userData['department']; ?>"  placeholder="Technical">
			</div>			
			<div class="form-group">
				<label>CTC</label>
				<input type="text" class="form-control" id="ctc" name="ctc" value="<?php echo $userData['ctc']; ?>"  placeholder="1000000">
			</div>

			<div class="form-group">
				<label>Basic</label>
				<input type="text" class="form-control" id="ba" name="ba" value="<?php echo $userData['basic']; ?>"  placeholder="1000000">
			</div>
			<div class="form-group">
				<label>HRA</label>
				<input type="text" class="form-control" id="hra" name="hra" value="<?php echo $userData['hra']; ?>"  placeholder="1000000">
			</div>
			<div class="form-group">
				<label>Conveyance Allowance</label>
				<input type="text" class="form-control" id="ca" name="ca" value="<?php echo $userData['ca']; ?>" placeholder="1000000">
			</div>
			<div class="form-group">
				<label>Medical Allowance</label>
				<input type="text" class="form-control" id="ma" name="ma" value="<?php echo $userData['ma']; ?>" placeholder="1000000">
			</div>
			<div class="form-group">
				<label>Special Allowance</label>
				<input type="text" class="form-control" id="sa" name="sa" value="<?php echo $userData['sa']; ?>" placeholder="1000000">
			</div>

		  	<button type="submit" name="submit" class="btn btn-primary">Edit</button>
		</form>	
		<a href="generate-ol-pdf.php?user_id=<?php echo $_GET['user_id']; ?>"><button class="btn btn-primary">Generate PDF</button></a>
	</div>
	<div class="col-md-3"></div>		
</div>
<div class="col-md-12">&nbsp;</div>


<?php 
	include_once 'footer.php';
?>
