<?php	
	if( $_SERVER['HTTP_HOST'] == 'localhost' ) {
		$base_url = '';
	} else {
		$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/';
	}

	include_once 'Config.php';
	require_once 'common.php';
	require_once 'connection.php';
	require_once 'class.sqlWrappers.php';

?>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<title><?=$title ?></title>
	<meta name="description" content="<?=$meta_desc ?>">
</head>
<body>
<div><a href="create-offer-letter.php"><h3>Generate Offer Letter</h3></a></div>