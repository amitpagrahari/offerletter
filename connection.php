<?php
error_reporting(E_ALL);
define("SERV", "127.0.0.1");
define("PORT", "3306");
define("USERNAME", "root");
define("PASS", "amit@79");
// define("DATABASE","platform");

function get_Db_connection($dbName = '') {
    #----------------------------------
    #---We are using conn everywhere
    #---And conn is global everywhere 
    #---Why not declare it global here
    #---12-Mar 18:47
    #---Declaring global here is not useful
    #---Because in code it will create problem
    #---------------------------------------
    #--global $conn;
    global $database;
    $database = "offerletter";
    if( !empty($dbName) ) {
    	$database = $dbName;
    }
    $conn = mysqli_connect(SERV, USERNAME, PASS, $database, PORT);
    if ( mysqli_connect_errno() ) {
    	error_log(date("Y-m-d H:i:s").':: mysql error : '.mysqli_connect_error().':: mysql error no : '.mysqli_connect_errno());
	  return false;
	}
    return $conn;
}

function close_Db_connection($conn) {
	#-------------------------------
	#---In code somewhere we are using get_Db_connection function
	#---And somewhere $conn
	#---and somewhere we using close_Db_connection
	#---That is closing global connection
	#---------------------------------------
    mysqli_close($conn);
	return true;
}

function mysqlRetryLogic($query,$functionName='')
{
    global $conn, $database;
    $conn = get_Db_connection($database);
    mysqli_set_charset($conn,"utf8");
	$retryAttempt=1;
	if(constant("MYSQL_RETRY_NUMBER")!="")
	{
		$retryAttempt=MYSQL_RETRY_NUMBER;
	}
	$error="";
    $mysqlError="";
	for($i=0;$i<$retryAttempt;$i++)
	{
		$responseData = '';
		$responseData = mysqli_query($conn,$query);
	        if ($responseData === false) 
		{
            $mysqlError=mysqli_error($conn);
            if(strpos(strtoupper($mysqlError),strtoupper("MySQL server has gone away"))!==false)
            {
                sleep(1);
                $conn = get_Db_connection($database);
            }
	    else if(strpos(strtoupper($mysqlError),strtoupper("Access denied for user 'root'@'localhost'"))!==false)
	    {
		#---------------------------------------------------------------------------------------------------
		#---Applying this else if condition
		#---Because either if we use mysql_real_escape_string
		#---Or mysql_set_charset
		#---Or mysql_query("Set charset utf-8");
		#---It is trying to connect to mysql
		#---If no connection is made so it will try to make connection on the basis of default root\@localhost
		#---This is the problem
		#---We have also to identiy it 
		#---If this is the problem then we have to connect it once again
		#--------------------------------------------------------------------------------------------------------
		sleep(1);
		$conn = get_Db_connection($database);
	    }
			if($error=="")
			{
				$error="Iteration:$i::".$mysqlError;
			}
			else
			{
				$error.="; Iteration :$i::".$mysqlError;
			}
			continue;				
		}
		else
		{
            $mysqlError="";
			break;
		}
	}

    #--------------------------------------------
    #---If mysql server gone away
    #---Then after 1 second try to connect it
    #---------------------------------------------

    if(strpos(strtoupper($mysqlError),strtoupper("MySQL server has gone away"))!==false)
    {
        sleep(1);
        $conn = get_Db_connection($database);
    }

	if($error!="")
	{
		writeLog($functionName.":: Error : ".$error." ;For Query Is:".$query,"",4);
	}
	$retryAttempt = $mysqlError = $error = null;
	unset($retryAttempt,$mysqlError,$error);
	return $responseData;
}

?>
