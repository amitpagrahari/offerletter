<?php
	class sqlWrappers {
		
		function __construct($database = '')	{
			global $conn, $clauseList, $clauseListOther, $insertOrReplaceList;
			$conn = get_Db_connection($database);	
			$configObj = new Configs;
			$clauseList = $configObj->sqlClauseList();		
			$clauseListOther = $configObj->sqlClauseListOther();
			$insertOrReplaceList = $configObj->insertOrReplaceList();
		}

		/*
		function __destruct()	{
			global $conn, $clauseList, $clauseListOther;
			$conn = $clauseList = $clauseListOther = null;
			unset($conn, $clauseList, $clauseListOther);
		}
		*/


		/*
		$tableParam = array(
						'oa_adv_offer_time' => 
							array (
								'get_cols' => array('default_payout','currency',array('column_name'=>'currency','func'=>'DISTINCT')),
								'where_cols' => array(
														array('cols' => 'conversion_cap' , 'val' => 0, 'cond'=> '=','func'=>'TRIM' ),
														array(
                                                            'cols'=> array(
                                                                array('cols' => 'offer_url' , 'val' => '%not.%', 'cond'=> 'LIKE'),
                                                                array('cols' => 'advertiser_name' , 'val' => '%NOT TO BE UPDATED%', 'cond'=> 'LIKE')
                                                                ),
                                                            'operators' => array('OR')
                                                            )
													)
								),
						'oa_advertiser_info' => 
							array (
								'get_cols' => array('aggregator_id','hasoffer_advertiser_id'),
								'where_cols' => array(
														array('cols' => 'conversion_cap' , 'val' => 0, 'cond'=> '=' ),
														array('cols' => 'conversion_cap2' , 'val' => 2, 'cond'=> '>=' )
													)
								)
						);

	    $joinsName = array( 
					array('name' => array('LEFT JOIN'=>'aggregator_id,id'),
						'cond'=> array( 
									array(
										'oa_advertiser_info' => 
											array('cols' => 'conversion_cap' , 'val' => 0, 'cond'=> '=','operator'=>'AND'),
											array()
										),
									array(
										'oa_adv_offer_time' => 
											array('cols' => 'conversion_cap' , 'val' => 0, 'cond'=> '=','operator'=>'AND'),
											array()
										)
								)
						)
				);
	    $whereOperator  = array('AND','OR','AND');
		$orderCols = array('table_name'=>'oa_advertiser_info','column_name'=>'column_name','sort'=>'desc');
		$limit = 10;
		$offset = 2;
		$selectType = array('table_name'=>'oa_advertiser_info','column_name'=>'aggregator_id','type' => 'COUNT');
		$groupBy = array('table_name'=>'oa_advertiser_info','column_name'=>'aggregator_id');
		
		*/
		function sqlFetch($tableParam, $whereOperator = '', $joinsName = '', $groupBy = '', $orderCols = '', $limit = '', $offset = '', $selectType = '') {
			global $conn, $clauseList, $clauseListOther;
			if( empty($tableParam) ) {
				writeLog('table param not set' ,__FILE__.'::'.__FUNCTION__,3);
				return false;
			}
			if( count($tableParam) > 0 ) {
				$sqlQuery = '';
				$i = 0;
				$tables = $cols = $whereCols = array();
				foreach ($tableParam as $key => $value) {
					$i++;
					$tables[$key] = 'tbl'.$i;
					if( !empty($value['get_cols']) ) {
						foreach ($value['get_cols'] as $getColsVal ) {
							if( is_array($getColsVal) ) {
								if( !isset($getColsVal['column_name']) || $getColsVal['column_name'] == '' || $getColsVal['column_name'] == '*' ) {
									if( strtolower($getColsVal['func']) == 'count' ) {
										$cols[] = $getColsVal['func']."(*) AS ".strtolower($getColsVal['func']);
									} else {
										$cols[] = $getColsVal['func']."(*) AS ".$getColsVal['column_name'];
									}
								} else {
									if( strtolower($getColsVal['func']) == 'count' ) {
										$cols[] = $getColsVal['func']."(tbl".$i.".".$getColsVal['column_name'].") AS ".strtolower($getColsVal['func']);
									} else {
										$cols[] = $getColsVal['func']."(tbl".$i.".".$getColsVal['column_name'].") AS ".$getColsVal['column_name'];
									}
								}
							} else {
								$cols[] = "tbl".$i.".".$getColsVal." AS ".$getColsVal;
							}
							$getColsVal = null;
							unset($getColsVal);
						}
					}
					if( !empty($value['where_cols']) ) {
						$whereCols = array_merge($this->getWhereClauseArray("tbl".$i,$value['where_cols']),$whereCols);
					}
					$key = $value = null;
					unset($key, $value);
				}
				if( count($tables) > 0 ) {
					$sqlQuery = '';
				}
				$joinTable = $this->getTableJoins($tables,$joinsName);
				$whereCondition = $this->getWhereConditions($whereCols,$whereOperator);
				if( empty($cols) ) {
					if( $selectType != '' && !empty($selectType) ) {
						$sqlQuery .= "SELECT ".$selectType['type']."(".$tables[$selectType['table_name']].'.'.$selectType['column_name'].") AS ".strtolower($selectType['type'])." FROM ".$joinTable;
					} else {
						$sqlQuery .= "SELECT * FROM ".$joinTable;
					}
				} else {
					$sqlQuery .= "SELECT ".implode(',', $cols)." FROM ".$joinTable;
				}
				if( count($whereCols) ) {
					$sqlQuery .=" WHERE ".$whereCondition;
				}
				if( $groupBy != '' && !empty($groupBy) ) {
					$sqlQuery .= ' GROUP BY '.$tables[$groupBy['table_name']].'.'.$groupBy['column_name'];
				}
				if( $orderCols != '' && !empty($orderCols) ) {
					$sqlQuery .= ' ORDER BY '.$tables[$orderCols['table_name']].'.'.$orderCols['column_name'].' '.$orderCols['sort'];
				}
				if( $limit != '' && !empty($limit) ) {
					if( $offset != '' && !empty($offset) ) {
						$sqlQuery .= ' LIMIT '.$offset.','.$limit;
					} else {
						$sqlQuery .= ' LIMIT '.$limit;
					}
				}
				writeLog('Build Query is '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);//return true;
				$resultQueryResult = $this->runQuery( $sqlQuery );
				if( count($resultQueryResult) > 0 ) {
					writeLog('Gettin Data, count is :: '.count($resultQueryResult).' and query '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);
				} else {
					writeLog('Getting No Data from '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);
				}
				$whereCondition = $joinTable = $sqlQuery = $i = $tables = $cols = $whereCols = $tableParam = $whereOperator = $joinsName = $groupBy = $orderCols = $limit = $offset = $selectType = null;
				unset($joinTable,$sqlQuery,$i,$tables,$cols,$whereCols,$tableParam,$whereOperator,$joinsName,$groupBy,$orderCols,$limit,$offset,$selectType);
				return $resultQueryResult;
			} else {
				writeLog('table param not set' ,__FILE__.'::'.__FUNCTION__,3);
				return false;
			}
		}
		/*
			Get Param sample details for function sqlInsert
			$table = 'table_name';
			$cols = array('col1','col2');
			$values = array( array('val1','val2'),
							 array('val1','val2')
							); 
			$onDuplicate = array( 'col1'=>'val1','col2'=>'val2');
		*/
		function sqlInsertOrReplace($insertOrReplace,$table,$cols,$values,$onDuplicate = '' ) {
			global $conn,$insertOrReplaceList;
			if( count(preg_grep('/'.$insertOrReplace.'/i', $insertOrReplaceList)) == 0 ) {
				writeLog('Add valid insertOrReplace values' ,__FILE__.'::'.__FUNCTION__,3);
				$insertOrReplace = $table = $cols = $values = $onDuplicate = null;
				unset($insertOrReplace,$table,$cols,$values,$onDuplicate);
				return false;
			}
			if( empty($table) && empty($values) ) {
				writeLog('table or values param not set' ,__FILE__.'::'.__FUNCTION__,3);
				$insertOrReplace = $table = $cols = $values = $onDuplicate = null;
				unset($insertOrReplace,$table,$cols,$values,$onDuplicate);
				return false;
			}
			$sqlQuery = '';
			$getVals = array();
			foreach ($values as $valKey => $val) {
				$getVals[] = "('".implode('\',\'', $val)."')";
				$valKey = $val = null;
				unset($val,$valKey);
			}
			if( empty($cols) ) {
				$colsItem = '';
			} else {
				$colsItem = "(".implode(',', $cols).")";
			}
			$sqlQuery = $insertOrReplace." INTO ".$table." ".$colsItem." VALUES ".implode(',', $getVals);
			$getDupData = '';
			if( $onDuplicate != '' && !empty($onDuplicate) ) {
				foreach ($onDuplicate as $dupKey => $dupVal) {
					$getDupData[] = $dupKey."='".mysqli_real_escape_string($conn,$dupVal)."'";
				}
				$sqlQuery .= " ON DUPLICATE KEY UPDATE ".implode(',', $getDupData);
			}
			writeLog('Build Query is '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);//return true;
			$getResult = mysqlRetryLogic($sqlQuery,__FUNCTION__);
			if( $getResult == false ) {
				writeLog('Unable to Insert '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);
			} else {
				writeLog('Query Succesfully Inserted  for '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);
			}
			$insertOrReplace = $table = $cols = $values = $onDuplicate = $colsItem = null;
			unset($insertOrReplace,$table,$cols,$values,$onDuplicate,$colsItem);
			return $getResult;
		}
		/*
			Get Param sample details for function sqlUpdate
			$table = 'table_name';
			$updateVal = array( 'col1'=>'val1','col2'=>'val2');
			$where = array( 'cond' => array('col1'=>'val1','col2'=>'val2','col2'=>array('IN'=>'"'.$offerId.'"')),'opr' => array('AND'));
		 */
		function sqlUpdate( $table,$updateVal,$where = '' ) {
			global $conn;
			if( empty($table) && empty($updateVal) ) {
				$table = $updateVal = $where = null;
				unset($table,$updateVal,$where);
				writeLog('table or update values param not set' ,__FILE__.'::'.__FUNCTION__,3);
				return false;
			}
			$sqlQuery = '';
			$setVals = array();
			foreach ($updateVal as $valKey => $val) {
				$setVals[] = $valKey."='".$val."'";;
				$valKey = $val = null;
				unset($val,$valKey);
			}
			$sqlQuery = "UPDATE ".$table." SET ".implode(',', $setVals);
			$whereData = '';
			if( $where != '' ) {
				$i = 0;
				foreach ( $where['cond'] as $upKey => $upVal ) {
					if( $i == 0 ) {
						$whereData .= $upKey."='".$upVal."'";
					} else if ( is_array($upVal) ) {
						$whereData .= " ".$where['opr'][$i-1]." ".$upKey." ".array_keys($upVal)[0]."(".array_values($upVal)[0].")";
					} else {
						$whereData .= " ".$where['opr'][$i-1]." ".$upKey."='".$upVal."'";
					}					
					$i++;
				}
				$sqlQuery .= " WHERE ".$whereData;
			}
			writeLog('Build Query is '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);//return true;
			$getResult = mysqlRetryLogic($sqlQuery,__FUNCTION__);
			if( $getResult == false ) {
				writeLog('Unable to Update '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);
			} else {
				writeLog('Updated Sucessfully for '.$sqlQuery ,__FILE__.'::'.__FUNCTION__,3);
			}
			$table = $updateVal = $where = null;
			unset($table,$updateVal,$where);
			return $getResult;
		}

		function getWhereClauseArray($tableAlias,$whereClauseCols){
			global $clauseList,$clauseListOther;
			$whereCols = array();
			foreach ( $whereClauseCols as $whereColsVal ) {
				if( isset($whereColsVal['func']) && !empty($whereColsVal['func']) ) {
					$whereCols[] = $whereColsVal['func']."(".$tableAlias.".".$whereColsVal['cols'].") ".$whereColsVal['cond']." '".$whereColsVal['val']."'";
				} else if ( is_array($whereColsVal['cols']) ) {
					$getWhereExtraData = $this->getWhereClauseArray($tableAlias,$whereColsVal['cols']);
					$whereCols[] = "(".$this->getWhereConditions($getWhereExtraData,$whereColsVal['operators']).")";
				} else {
					if( in_array($whereColsVal['cond'], $clauseList) ) {
						$whereCols[] = $tableAlias.".".$whereColsVal['cols']." ".$whereColsVal['cond']." (".$whereColsVal['val'].")";
					} else if ( in_array($whereColsVal['cond'], $clauseListOther) ){
						$whereCols[] = $tableAlias.".".$whereColsVal['cols']." ".$whereColsVal['cond']." ".$whereColsVal['val'];
					} else {
						$whereCols[] = $tableAlias.".".$whereColsVal['cols']." ".$whereColsVal['cond']." '".$whereColsVal['val']."'";
					}
				}
				$whereColsVal = null;
				unset($whereColsVal);
			}
			$tableAlias = $whereClauseCols = null;
			unset($tableAlias,$whereClauseCols);
			return $whereCols;
		}
		
		function getWhereConditions( $whereCols,$whereOperator ) {
			$count = count($whereOperator);
			$whereVal = '';
			if( $count == 0 ) {
				$whereVal = implode(' AND ', $whereCols);
			} else if ( count($whereCols) == 1 ) {
				$whereVal = $whereCols[0];
			} else {
				if( $count == (count($whereCols) - 1) ) {
					foreach ($whereCols as $whereColsKey => $whereColsValue) {
						if( $whereColsKey < $count ) {
							$whereVal .= $whereColsValue.' '.$whereOperator[$whereColsKey].' ';
						} else {
							$whereVal .= ' '.$whereColsValue;
						}
						$whereColsKey = $whereColsValue = null;
						unset($whereColsKey,$whereColsValue);
					}
				} else {
					writeLog('whereOperator variable count should be one less than total where condition' ,__FILE__.'::'.__FUNCTION__,3);
					$whereCols = $whereOperator = null;
					unset($whereCols,$whereOperator);
					return false;
				}
			}
			$whereCols = $whereOperator = null;
			unset($whereCols,$whereOperator);
			return $whereVal;
		}

		function getTableJoins( $tables,$joinsName ) {
			$count = count($joinsName);
			$tableVal = '';
			if( $count == 0 || count($tables) == 1 ) {
				$tableVal = key($tables)." AS ".$tables[key($tables)];
			} else {
				if( $count == (count($tables) - 1) ) {
					$i = 0;
					foreach ($tables as $tablesColsKey => $tablesColsValue) {
						if( $i == 0 ) {
							$tableVal .= " ".$tablesColsKey.' AS '.$tablesColsValue." ";
						} else {
							$getNextTbl = array_values($tables)[$i-1];
							$getJoinCond = explode(',', $joinsName[$i-1]['name'][key($joinsName[$i-1]['name'])]);
							$firstJoinVal = $secJoinVal = '';
							if( count($getJoinCond) == 2 ) {
								$firstJoinVal = $getJoinCond[0];
								$secJoinVal = $getJoinCond[1];
							} else {
								$secJoinVal = $firstJoinVal = $getJoinCond[0];
							}
							$tableVal .= " ".key($joinsName[$i-1]['name']).' '.$tablesColsKey.' AS '.$tablesColsValue.' ON '.$getNextTbl.'.'.$firstJoinVal.'='.$tablesColsValue.'.'.$secJoinVal;
							if( !empty($joinsName[$i-1]['cond']) ) {
								$tableVal .= " ".$this->getJoinExtraCond($tables , $joinsName[$i-1]['cond']);
							}
						}
						$i++;
						$tablesColsKey = $getNextTbl = $tablesColsValue = $getJoinCond = null;
						unset($tablesColsKey, $tablesColsValue,$getNextTbl,$getJoinCond);	
					}
				} else {
					writeLog('joinsName variable count should be one less than total table variable count' ,__FILE__.'::'.__FUNCTION__,3);
					$tables = $joinsName = null;
					unset($tables,$joinsName);
					return false;
				}
			}
			$count = $tables = $joinsName = null;
			unset($count,$tables,$joinsName);
			return $tableVal;
		}
		function getJoinExtraCond($tables , $conditions) {
			$extraCond = '';
			foreach ( $conditions as $vals ) {
				foreach ($vals as $key => $value) {
					if( !empty($value) ) {
						$extraCond .= $value['operator']." ".$tables[$key].".".$value['cols']." ".$value['cond']." '".$value['val']."' ";
					}
					$key = $value = null;
					unset($key,$value);
				}
				$vals = null;
				unset($vals);
			}
			$tables = $conditions = null;
			unset($tables , $conditions);
			return $extraCond;
		}
		function runQuery( $sqlQuery ) {
			global $conn;
			if( empty($sqlQuery) ) {
				writeLog('sqlQuery values param not set' ,__FILE__.'::'.__FUNCTION__,3);
				$sqlQuery = null;
				unset($sqlQuery);
				return false;
			}
			$resultQueryResult = '';
			$resultQueryResult = mysqlRetryLogic($sqlQuery,__FUNCTION__);
			$outData = array();
			while ( $row = mysqli_fetch_assoc($resultQueryResult) ) {
				$outData[] = $row;
				$row = null;
				unset($row);
			}
			close_Db_connection($conn);
			return $outData;
		}
	}

?>