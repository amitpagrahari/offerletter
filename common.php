<?php 

function writeLog($data, $subject = "", $logLevel = 0) {
    global $linuxProcessId;
    if (!isset($linuxProcessId)) $linuxProcessId = getmypid();

    if (LOG_LEVEL_FOR_WRITE <= $logLevel) {
        error_log(date("Y-m-d H:i:s") . '::' . $subject . '::' . $linuxProcessId . '::' . print_r($data, true) . PHP_EOL);
    }
}

?>